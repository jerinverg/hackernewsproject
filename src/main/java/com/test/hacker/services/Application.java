package com.test.hacker.services;

import com.test.hacker.exceptions.ServicesExceptionMapper;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")
public class Application extends ResourceConfig {

    public Application() {
        packages("io.swagger.v3.jaxrs2.integration.resources,com.test.hacker.services");

        //register(JacksonFeatures.class);
        register(ServicesExceptionMapper.class);
    }

}
